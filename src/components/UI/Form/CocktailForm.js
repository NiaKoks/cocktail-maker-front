import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import "./CocktailForm.css"
class CocktailForm extends Component {
    state={
        name:'',
        ingredients:[{name:'',ammount:''}],
        recepie:'',
        image: null
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    ingredientInputChangeHandler=(event,index)=>{
        const ingredient ={...this.state.ingredients[index]};

        ingredient[event.target.name] = event.target.value;

        const ingredients =[...this.state.ingredients];
        ingredients[index] = ingredient;

        this.setState({ingredients})
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (key === 'ingredients') {
                formData.append(key, JSON.stringify(this.state[key]));
            } else
            formData.append(key, this.state[key]);
        });
        this.props.onSubmit(formData);
    };
    addIngredient = event =>{
        event.preventDefault();
        this.setState({
            ingredients: [...this.state.ingredients,{name:'',ammount:''}]
        })
    };
    deleteIngredient = index =>{
        const ingredients = [...this.state.ingredients];
        ingredients.splice(index,1);
        this.setState({ingredients});

    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler} sm={10}>
                <FormGroup row>
                    <Label>Name:</Label>
                    <Col>
                        <Input
                            type="text"
                            name="name" id="name"
                            placeholder="Input name of the cocktail"
                            value={this.state.name}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label>Ingredients:</Label>
                    <Col>
                        {this.state.ingredients.map((ingredient,index) =>(
                            <FormGroup key={index} className="ingredients">
                                <Label>Name of ingredient:</Label>
                                <Input type="text" name="name"
                                    onChange={(event)=>this.ingredientInputChangeHandler(event,index)}
                                />
                                <Label>Ammount:</Label>
                                <Input type="text" name="ammount"
                                    onChange={(event)=>this.ingredientInputChangeHandler(event,index)}
                                />
                                {index > 0 &&
                                <Button type="button" onClick={this.deleteIngredient}> X </Button>
                                }
                            </FormGroup>
                        ))}
                        <Button type="button" onClick={this.addIngredient}> Add Ingredient </Button>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label>Recepie:</Label>
                    <Col>
                        <Input
                            type="textarea"
                            name="recepie" id="recepie"
                            placeholder="Input recepie"
                            value={this.state.recepie}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="img">Cocktail Image:</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="img" id="img"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col>
                        <Button type="submit" color="primary">Mix cocktail</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default CocktailForm;