import React ,{Fragment} from 'react';
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink,
    UncontrolledDropdown
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import FacebookLogin from "../../FacebookLogin/FacebookLogin";
import "./Toolbar.css"
const Toolbar = ({user,logout}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Cocktail Mixer</NavbarBrand>

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>All Cocktails</NavLink>
                </NavItem>

                {user ? (
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            <div className="avatar">
                                <img src={user.avatar} alt=""/>
                            </div>
                            Hello, {user.displayName}!
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/cocktails/new">
                                    Add New Cocktail
                                </NavLink>
                            </DropdownItem>
                            <DropdownItem>
                                <NavLink tag={RouterNavLink} to="/mycocktails">
                                    My Cocktails
                                </NavLink>
                            </DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem onClick={logout}>
                                Logout
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                ) :(
                    <Fragment>
                        <FacebookLogin/>
                    </Fragment>
                )}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;