import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";
import Thumbnail from "../Thumbnail/Thumbnail";
import {propTypes} from "reactstrap/es/TooltipPopoverWrapper";

const CocktailCard = props => {
    return (
        <Card style={{marginTop: '10px'}}>
            <CardBody>
                <Thumbnail image={props.image}/>
                <Link to={'/cocktails/' + props._id}>
                    {props.name}
                </Link>
                <div style={{marginLeft: '10px'}}>
                    {props.ingredients.map((ingredient,index) => {
                        return <ul key={index}> <li>{props.ingredientName} : {props.ingredientAmmount}</li></ul>
                    })}
                </div>
                <div>
                 <h5>Recepie:</h5>
                 <p>{props.recepie}</p>
                </div>
                {props.user && props.user.role ==='admin' &&
                <Fragment>
                    {props.published === false && <p>Need to be approved</p>}
                    <Button type="submit" color="success"
                         onClick={ props.published = this.setState({published:true})}
                    >
                        Post
                    </Button>
                    <Button
                        color="danger"
                        onClick={event=> { event.stopPropagation();
                            props.delete(props._id)
                        }}
                    >
                        Delete
                    </Button>
                </Fragment>
                }
            </CardBody>
        </Card>
    );
};
CocktailCard.propTypes={
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    name:PropTypes.string.isRequired,
    ingredients:PropTypes.array.isRequired,
    ingredientName: PropTypes.objectOf(propTypes.string),
    ingredientAmmount: PropTypes.objectOf(propTypes.string),
    published: PropTypes.bool
};
export default CocktailCard;
