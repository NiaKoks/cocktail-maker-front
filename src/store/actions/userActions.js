import {push} from 'connected-react-router';
import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';


const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS,user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const logoutUser = () => ({type:LOGOUT_USER});


export const facebookLogin = data =>{
    return dispatch =>{
        axios.post('/users/facebookLogin',data).then(
            response =>{
                console.log(response.data.user)
                dispatch(loginUserSuccess(response.data.user));
                dispatch(push('/'));
                NotificationManager.success('Logged in with Facebook!');
            },
            error =>{
                dispatch(loginUserFailure(error.response.data));
            }
        )
    };
};

