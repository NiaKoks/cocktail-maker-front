import {push} from 'connected-react-router';
import axios from '../../axios-api';

export const FETCH_HISTORY_SUCCESS = "FETCH_HISTORY_SUCCESS";

export const FETCH_COCKTAILS_SUCCESS = "FETCH_COCKTAILS_SUCCESS";
export const CREATE_COCKTAILS_SUCCESS = "CREATE_COCKTAILS_SUCCESS";


export const fetchHistorySuccess = tracks =>({type:FETCH_HISTORY_SUCCESS,tracks});
export const fetchCocktailSuccess = cocktails =>({type:FETCH_COCKTAILS_SUCCESS,cocktails});
export const createCocktailSuccess = cocktails =>({type:CREATE_COCKTAILS_SUCCESS,cocktails});

export const fetchCocktail = () =>{
  return dispatch =>{
      return axios.get('/cocktails').then(response => dispatch(fetchCocktailSuccess(response.data)))
  };
};

export const createCocktail = cocktailData =>{
    return dispatch =>{
        return axios.post('/cocktails',cocktailData).then(()=>dispatch(createCocktailSuccess()))
    }
};

export const deleteCocktail = (id,cocktailID)=>{
  return dispatch =>{
      return axios.delete(`/cocktails/${id}`).then(()=>dispatch(fetchCocktail(cocktailID)))
  }
};

export const fetchHistory = () =>{
    return (dispatch, getState) =>{
        const user = getState().users.user;
        if (user === null){
            dispatch(push('/login'))
        } else {
            return axios.get(`/tracks_history`, {headers:{'Authorization': user.token}}).then(response => dispatch(fetchHistorySuccess(response.data)))
        }
    };
};