import React, {Component,Fragment} from 'react';
import {connect} from 'react-redux';
import CocktailCard from "../components/Cards/CocktailCard";
import {deleteCocktail, fetchCocktail} from "../store/actions/cocktailActions";

class AllCocktails extends Component {
    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchCocktails(id);
    }

    render() {
        return (
        <Fragment>
            {this.props.cocktails.map(cocktail =>(
                <CocktailCard className="CocktailCard"
                    key={cocktail._id}
                    _id ={cocktail._id}
                    name={cocktail.name}
                    image={cocktail.image}
                    ingredients={cocktail.ingredients}
                    ingredientName={cocktail.ingredients.name}
                    ingredientAmmount={cocktail.ingredients.ammount}
                    recepie={cocktail.recepie}
                    user={this.props.user}
                    delete={this.props.deleteCocktail}
                />
            ))}
        </Fragment>
        );
    }
}
const mapStateToProps = state =>{
  return{
      cocktails: state.cocktails.cocktails,
      user: state.users.user
  }
};
const mapDispatchToProps = dispatch =>{
  return{
      fetchCocktails: () => dispatch(fetchCocktail()),
      deleteCocktail: (id,cocktailID) => dispatch(deleteCocktail(id,cocktailID))
  }
};
export default connect(mapStateToProps,mapDispatchToProps)(AllCocktails);