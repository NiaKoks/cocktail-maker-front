import React, {Component,Fragment} from 'react';
import {connect} from "react-redux";
import {createCocktail} from "../store/actions/cocktailActions";
import CocktailForm from "../components/UI/Form/CocktailForm";

class AddCocktail extends Component {

    createCocktail = cocktailsData => {
        this.props.onCocktailMaked(cocktailsData).then(() => {
            this.props.history.push('/');
        });
    };
    render() {
        return (
            <Fragment>
                <h2>Add new cocktail</h2>
                <CocktailForm onSubmit={this.createCocktail}/>
            </Fragment>
        );
    }
}
const mapDispatchToProps = dispatch =>({
   onCocktailMaked: cocktailsData => dispatch(createCocktail(cocktailsData)),
});
export default connect(null,mapDispatchToProps)(AddCocktail);