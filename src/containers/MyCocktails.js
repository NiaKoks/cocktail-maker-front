import React, {Component,Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchHistory} from "../store/actions/cocktailActions";
class MyCocktails extends Component {
    componentDidMount() {
        this.props.fetchHistory();
    }
    render() {
        return (
            <Fragment>
                <h2>Your cocktails</h2>
                {this.props.cocktails && this.props.cocktails.map(
                    (cocktail,index) =>{
                        return(
                            <Fragment>
                                <h4>cocktail.user</h4>
                                {cocktail.name}
                            </Fragment>)
                    })

                }
            </Fragment>
        );
    }
}
const mapStateToProps = state =>{
    return{
        cocktails: this.cocktails.cocktails
    }
};
const mapDispatchToProps = dispatch =>{
    return{
        fetchHistory:()=>dispatch(fetchHistory())
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(MyCocktails);