import React, { Component,Fragment} from 'react';
import {Route,Switch,withRouter} from "react-router-dom";
import {NotificationContainer} from 'react-notifications';
import Toolbar from "../src/components/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/userActions";
import AddCocktail from "./containers/AddCocktail";
import AllCocktails from "./containers/AllCocktails";
import MyCocktails from "./containers/MyCocktails";
import CocktailCard from "./components/Cards/CocktailCard";


class App extends Component {
  render() {
    return (
        <Fragment>
          <header><Toolbar user={this.props.user} logout={this.props.logout}/></header>
          <NotificationContainer/>
          <Switch>
            <Route path="/" exact component={AllCocktails}/>

            <Route path="/cocktails/new" exact component={AddCocktail}/>

            <Route path="/cocktails/:id" exact component={CocktailCard}/>

            <Route path="/mycocktails" exact component={MyCocktails}/>
          </Switch>
        </Fragment>
    );
  }
}

const mapStateToProps = state =>({
  user: state.users.user
});
const mapDispatchToProps = dispatch =>({
  logout: () => dispatch(logoutUser())
});
export default  withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
